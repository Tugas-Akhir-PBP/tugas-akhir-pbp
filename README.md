## Nama Anggota Kelompok:
2006487484	Ananda Fadhil Eka Prakoso <br>
2006528080	Michael Wong <br>
2006530526	Said Abdurrahman <br>
2006526541	Putri Martha Natasha <br>
2006523496	Raihan Fitra Setyabudi <br>
2006523003	Aprilian Tantra Luhur Achmad <br>
2006528603	Kevin Razaqa Aulia <br>
<br>

# APK Download Link:
https://drive.google.com/drive/folders/1VNYFV6xnw27xOBBi-O0PfZvD31o89354?usp=sharing <br>

**Cerita Daftar Modul dan Implementasinya:** <br>
## Deskripsi Aplikasi:
Pada masa pandemi Covid-19 ini, kita diperkenalkan dengan sistem PJJ atau istilah lainnya *Work From Home*. Kita diharuskan untuk mengerjakan segala sesuatu sebisa mungkin dari rumah masing-masing mulai dari belajar, bekerja, beribadah, dan lain-lain. Selain itu, kita menjadi lebih sering berinteraksi dengan *device* yang kita miliki. Oleh karena itu, untuk mendukung dan mempermudah kinerja pada saat ini, kelompok kami membuat suatu aplikasi yang bernama 'Reminder'. Aplikasi ini menampilkan beberapa fitur 'reminder' untuk mengingatkan kita akan *assignment* dan hal-hal lainnya yang berkaitan dengan perkuliahan (terutama dalam Fasilkom). Tidak hanya fitur pengingat, terdapat juga fitur catatan dan simpan kontak untuk menyimpan kontak penting yang dapat kita hubungi apabila ingin berkomunikasi dengan dosen atau asisten dosen.<br>

# List Modul 
**Modul kelompok kami terdiri dari :**<br>

_Login & Landing Page_ : Michael Wong <br>
_Contact Person_ : Said Abdurrahman <br>
_Deadline Table_ : Ananda Fadhil <br>
_Notes_ : Kevin Razaqa <br>
_To Do List_ : Putri Martha <br>
_Review Page_ : Aprilian Tantra <br>

# Cerita Modul
1. Modul _Login_ dan _Landing Page_ <br>
Modul ini akan memuat halaman _login page_ untuk *user* ketika membuka aplikasi pertama kali. Setelah *user* _login_, *user* akan diarahkan ke halaman _landing page_ yang memuat _widget-widget_ untuk masing-masing modul yang akan ada di dalam aplikasi.
2. Modul _Contact Person_ <br>
Modul ini berisi informasi kontak Dosen/Asdos mengenai nama, mata kuliah, dan emailnya. Terdapat sebuah input yang dapat *user* isi untuk mencatat kontak Dosen/Asdos. 
3. Modul _Deadline Table_ <br>
Modul ini berisi tentang tabel atau detail deadline dari tugas yang dapat *user* masukkan melalui *form input*. Nantinya, tabel ini dapat dihapus oleh pengguna apabila *deadline* sudah selesai. Namun, bentuk tabel diganti menjadi bentuk *card* pada tampilan *mobile* agar lebih cocok. <br>
4. Modul _Notes_ <br>
Modul ini berisi catatan dari mata kuliah *user* yang dapat ditambahkan melaluti *form input*. Catatan ini nantinya dapat diakses dan dilihat oleh *user* dan dapat disort berdasarkan mata kuliah yang *user* inginkan.
5. Modul _To Do List_ <br>
Modul ini berisi sebuah form yang berguna untuk menerima input dari *user* untuk mencatat kegiatan yang ingin dilakukan. Terdapat juga sebuah input untuk memasukkan batas tanggal untuk kegiatan yang telah ditulis. Selain itu, terdapat tombol delete yang berguna untuk menghapus kegiatan yang sudah selesai dilakukan. <br>
6. Modul _Review Page_ <br>
Modul ini berisi sebuah form yang menerima input dari *user* berupa teks yang mencatat nama dan komen dari *user* terkait app kami. Selain itu pada page ini *user* juga akan memberikan input berupa radiobutton dari 1 - 5 yang merupakan ratings yang diberikan untuk app kami. Semua input tersebut akan tersimpan di database. Semua yang terintegrasikan di website akan diimplementasikan ke dalam bentuk aplikasi. <br>
7. Modul _Navbar_ <br>

Tampilan dari aplikasi _mobile_ kami akan mengacu pada tampilan halaman _web_ kami pada Proyek Tengah Semester. Setiap modul aplikasi _mobile_ yang kami buat akan mengakses _database_ dari aplikasi _web_ Django kami. Kami akan menambahkan beberapa fungsi pada _views.py project Django_ kami yang akan mereturn *JsonResponse* yang nantinya akan dipanggil oleh Proyek Tugas Akhir yaitu _Flutter_.
