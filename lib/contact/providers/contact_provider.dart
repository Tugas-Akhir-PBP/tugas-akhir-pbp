import '../models/contact_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ContactProvider extends ChangeNotifier {
  List<ContactModel> _data = [];
  List<ContactModel> get data => _data;
  Future<List<ContactModel>> getContact() async {
    final url = 'https://reminder-tk-pbp.herokuapp.com/contact/getdosen';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final result =
          json.decode(response.body)[''].cast<Map<String, dynamic>>();
      _data = result
          // .map<ContactModel>((json) => ContactModel.fromJson(json))
          .toList();
      print(_data);
      return _data;
    } else {
      throw Exception();
    }
  }
}
