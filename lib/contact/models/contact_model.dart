// ignore: empty_constructor_bodies
// ignore_for_file: empty_constructor_bodies

import 'dart:convert';

class ContactModel {
  String name;
  String email;
  String role;
  String matkul;
  // "name": "Said Abdurrahman",
  // "email": "said.abdurrahman@ui.ac.id",
  // "role": "Asdos",
  // "matkul": "PBP",

  ContactModel({
    required this.name,
    required this.email,
    required this.matkul,
    required this.role,
  }) {}
  // factory ContactModel.fromJson(Map<String, dynamic>) => ContactModel(
  //   name: json['name'],
  //   email: json['email'],
  //   matkul: json['matkul'],
  //   role: json['role'], 
  //   );
}
