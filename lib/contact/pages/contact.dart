import 'package:flutter/material.dart';
import './contact_add.dart';
import '../models/contact_model.dart';

  final data = [
    ContactModel(name: "Said Abdurrahman", email: "said.abdurrahman@ui.ac.id", role: "Asdos", matkul: "PBP" ),
    ContactModel(name: "Putri Martha", email: "putri.martha@ui.ac.id", role: "Asdos", matkul: "PBP" ),
  ];
class Contact extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Contact List')),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.grey[600],
        child: Text('+', style: TextStyle(fontSize: 35)),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ContactAdd()));
        },
      ),
      body: Container(
          margin: EdgeInsets.all(10),
          child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, i) {
              return Card(
                elevation: 8,
                child: ListTile(
                  title: Text(data[i].name),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    Text(data[i].email),
                    Text(data[i].matkul),
                  ],),
                  trailing: Text(data[i].role),
                ),
              );
            },
          )),
    );
  }
}
