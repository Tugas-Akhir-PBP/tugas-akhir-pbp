import 'package:flutter/material.dart';
import 'package:reminder/contact/models/contact_model.dart';
import './contact.dart';

class ContactAdd extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ContactAddState();
  }
}

class ContactAddState extends State<ContactAdd> {
  String _name = "Said Abdurrahman";
  String _email = "said.abdurrahman@ui.ac.id";
  String _role = "Asdos";
  String _matkul = "PBP";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      maxLength: 20,
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Nama!';
        }
      },
      onSaved: (value) {
        _name = value.toString();
      },
    );
  }

  _buildEmail() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Email'),
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Email!';
        }
      },
      onSaved: (value) {
        _email = value.toString();
      },
    );
  }

  _buildRole() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Role'),
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Role!';
        }
      },
      onSaved: (value) {
        _role = value.toString();
      },
    );
  }

  _buildMatkul() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Mata Kuliah'),
      validator: (value) {
        if (value.toString().isEmpty) {
          return 'Wajib memasukan Mata kuliah!';
        }
      },
      onSaved: (value) {
        _matkul = value.toString();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Write Contact"),
        ),
        body: Container(
          margin: EdgeInsets.all(24),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildName(),
                _buildEmail(),
                _buildRole(),
                _buildMatkul(),
                SizedBox(height: 100),
                RaisedButton(
                  child: Text('Submit',
                      style: TextStyle(color: Colors.blueAccent, fontSize: 16)),
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }
                    _formKey.currentState!.save();
                    data.add(ContactModel(
                        name: _name,
                        email: _email,
                        role: _role,
                        matkul: _matkul));
                    print(_name);
                    print(_email);
                    print(_role);
                    print(_matkul);
                    print(data);
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => Contact()));
                  },
                ),
              ],
            ),
          ),
        ));
  }
}
