import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:core';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() => runApp(MaterialApp(
      title: "To Do",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.blue,
          accentColor: Colors.orange),
      home: ToDoApp(),
    ));

class ToDoApp extends StatefulWidget {
  @override
  _ToDoAppState createState() => _ToDoAppState();
}

class _ToDoAppState extends State<ToDoApp> {
  final _formKey = GlobalKey<FormState>();
  List todo = [];
  String input = "";
  String date = "";

  List<dynamic> todolist = [];

  //ngambil dari django
  Future getTodo() async {
    var task =
        await http.get(
          Uri.parse("https://reminder-tk-pbp.herokuapp.com/todolist/gettodo"));
    var task2 = json.decode(task.body);

    for (int i = 0; i < task2.length; i++) {
      var text = task2[i]['fields']['text'];
      //print(text);
      todolist.add(text);
    }
  }

  // @override
  // void initState() {
  //   super.initState();
  //   todo.add("Makan");
  //   todo.add("Tidur");
  //   todo.add("Mandi");
  //   todo.add("Olahraga");
  // }

  @override
  Widget build(BuildContext context) {
    //getTodo();
    return Scaffold(
        appBar: AppBar(
          title: Text("To Do List"),
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      title: Text("Add Todolist"),
                      content: TextFormField(
                        onChanged: (String value) {
                          input = value;
                        },
                      ),
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () async {
                              const url =
                                  "https://reminder-tk-pbp.herokuapp.com/todolist/addtodoflutter";
                              final haha = await http.post(Uri.parse(url),
                                  headers: <String, String>{
                                    "Content-Type":
                                        "application/json; charset=UTF-8",
                                  },
                                  body: jsonEncode(
                                      <String, String>{"text": input}));
                              Navigator.of(context).pop();
                              Navigator.push(
                                        context, MaterialPageRoute(builder: (_) =>ToDoApp()));
                            },
                            child: Text("Add"))
                      ],
                    );
                  });
            },
            child: Icon(
              Icons.add,
              color: Colors.white,
            )),
        body: Container(
            child: FutureBuilder(
                future: getTodo(),
                builder: (context, snapshot) {
                  return ListView.builder(
                      itemCount: todolist.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Dismissible(
                            key: Key(todolist[index]),
                            child: Card(
                              elevation: 4,
                              margin: EdgeInsets.all(8),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: ListTile(
                                title: Text(todolist[index]),
                                trailing: IconButton(
                                    icon: Icon(Icons.delete, color: Colors.red),
                                    onPressed: () {
                                      setState(() {
                                        //todolist[index]['fields']['complete'] = true;
                                        todolist.removeAt(index);
                                        
                                      });
                                    }),
                              ),
                            ));
                      });
                })));
  }
}
