import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/review_page.dart';
import 'package:http/http.dart' as http;

class ReviewPageProvider with ChangeNotifier {
  ReviewPageProvider() {
    fetchTasks();
  }
  List<ReviewPage> _reviews = [];

  List<ReviewPage> get reviews {
    return [..._reviews];
  }

  void addReview(ReviewPage review) async {
    final response = await http.post(
        Uri.parse('https://reminder-tk-pbp.herokuapp.com/apis/'),
        headers: {'Content-Type': 'application/json'},
        body: json.encode(review));
    if (response.statusCode == 201) {
      _reviews.add(review);
      notifyListeners();
    }
  }

  fetchTasks() async {
    final url = 'https://reminder-tk-pbp.herokuapp.com/apis/?format=json';
    final response = await http.get(Uri.parse(url), headers: {
      "Accept": "application/json",
      "Access-Control-Allow-Origin": "*"
    });
    if (response.statusCode == 200) {
      var data = json.decode(response.body) as List;
      _reviews =
          data.map<ReviewPage>((json) => ReviewPage.fromJson(json)).toList();
      notifyListeners();
    }
  }
}
