class ReviewPage {
  final String name;
  final String comment;
  final int score;

  ReviewPage({required this.name, required this.comment, required this.score});

  factory ReviewPage.fromJson(Map<String, dynamic> json) {
    return ReviewPage(
      name: json['name'],
      comment: json['comment'],
      score: json['score'],
    );
  }
  dynamic toJson() => {
        'name': name,
        'comment': comment,
        'score': score,
      };
}
