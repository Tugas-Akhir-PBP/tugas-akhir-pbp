// import 'dart:html';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:reminder/review_page/api/api.dart';
import './screens/add_review.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ReviewPageProvider(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: ReviewPages(),
      ),
    );
  }
}

class ReviewPages extends StatelessWidget {
  const ReviewPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final reviewP = Provider.of<ReviewPageProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Review Page'),
      ),
      body: ListView.builder(
        itemCount: reviewP.reviews.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(reviewP.reviews[index].name),
            subtitle: Text(reviewP.reviews[index].comment),
            trailing: Text(reviewP.reviews[index].score.toString()),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            size: 30,
          ),
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (ctx) => AddReviewScreen()));
          }),
    );
  }
}
