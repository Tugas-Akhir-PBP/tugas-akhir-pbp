// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:reminder/review_page/api/api.dart';
import 'package:reminder/review_page/models/review_page.dart';
import 'package:provider/provider.dart';

class AddReviewScreen extends StatefulWidget {
  @override
  _AddReviewScreenState createState() => _AddReviewScreenState();
}

class _AddReviewScreenState extends State<AddReviewScreen> {
  final nameControl = TextEditingController();
  final commentControl = TextEditingController();
  final scoreControl = TextEditingController();

  bool onAdd() {
    final String textVal = nameControl.text;
    final String comVal = commentControl.text;
    final String scoreVal = scoreControl.text;

    if (textVal.isNotEmpty &&
        comVal.isNotEmpty &&
        scoreVal.isNotEmpty &&
        isNumeric(scoreVal)) {
      final ReviewPage review = ReviewPage(
          name: textVal, comment: comVal, score: int.parse(scoreVal));
      Provider.of<ReviewPageProvider>(context, listen: false).addReview(review);
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Add Review')),
        body: ListView(
          children: [
            Container(
                child: Column(
              children: [
                const SizedBox(
                  height: 30,
                ),
                TextField(
                  decoration: const InputDecoration(
                    hintText: 'Masukkan nama Anda',
                    labelText: 'Nama',
                    border: OutlineInputBorder(),
                  ),
                  controller: nameControl,
                ),
                const SizedBox(
                  height: 30,
                ),
                TextField(
                  decoration: const InputDecoration(
                    hintText: 'Masukkan komentar Anda',
                    labelText: 'Komentar',
                    border: OutlineInputBorder(),
                  ),
                  controller: commentControl,
                ),
                const SizedBox(
                  height: 30,
                ),
                TextField(
                  decoration: const InputDecoration(
                    hintText: 'Masukan Angka 1-5',
                    labelText: 'Rating',
                    border: OutlineInputBorder(),
                  ),
                  controller: scoreControl,
                ),
                const SizedBox(
                  height: 30,
                ),
                RaisedButton(
                    child: Text("Submit"),
                    onPressed: () {
                      if (onAdd()) {
                        final message = 'Penambahan komentar berhasil!';
                        final snackBar = SnackBar(
                          content: Text(
                            message,
                            style: TextStyle(fontSize: 20),
                          ),
                          backgroundColor: Colors.blue,
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        Navigator.of(context).pop();
                      } else {
                        final message = 'Masukkan input yang valid!';
                        final snackBar = SnackBar(
                          content: Text(
                            message,
                            style: TextStyle(fontSize: 20),
                          ),
                          backgroundColor: Colors.blue,
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    })
              ],
            ))
          ],
        ));
  }
}

bool isNumeric(String s) {
  if (s == null) {
    return false;
  }
  return double.tryParse(s) != null;
}
