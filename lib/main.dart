import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reminder/contact/pages/contact.dart';
import 'package:reminder/deadline_table/deadline_table.dart';
import 'package:reminder/Login_&_Landing Page/login1.dart';
import 'package:reminder/Login_&_Landing Page/register.dart';
import 'package:reminder/review_page/models/review_page.dart';
import 'CookieRequest.dart';
// import 'package:tabel_fadhil/tabel_fadhil.dart';
import 'deadline_table/deadline_table.dart';
import 'package:reminder/Notes/note_main.dart';
import 'package:reminder/to_do_list/todolist.dart';
import 'to_do_list/todolist.dart';
import 'contact/contact.dart';
import 'review_page/review_page_main.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  static const String title = 'HomePage';
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Provider(
        create: (_) {
          CookieRequest request = CookieRequest();

          return request;
        },
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: MainPage(),
        ));
  }
}

class MainPage extends StatefulWidget {
  @override
  Home createState() => Home();
}

class Home extends State<MainPage> {
  int currentIndex = 0;
  final screens = [
    Center(
      child: Text(
        'Home',
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        'To Do List',
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        'Deadline',
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        'Catatan',
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        'Contact Person',
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        'Review',
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        'Login',
        style: TextStyle(fontSize: 30),
      ),
    ),
    Center(
      child: Text(
        'Register',
        style: TextStyle(fontSize: 30),
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('REMINDER'),
          centerTitle: true,
        ),
        body: screens[currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: currentIndex,
          onTap: (index) {
            if (index == 2) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => DeadlineTable(
                            title: 'DeadlineTable',
                          )));
            } else if (index == 3) {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => DaftarCatatan()));
            } else if (index == 1) {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => ToDoApp()));
            } else if (index == 6) {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => LoginPages()));
            } else if (index == 7) {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => RegisterPages()));
            } else if (index == 4) {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => Contact()));
            } else if (index == 5) {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => ReviewPages()));
            } else {
              setState(() {
                currentIndex = index;
              });
            }
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'HOME',
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.lock_clock),
              label: 'To Do List',
              backgroundColor: Colors.red,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.lock_clock),
              label: 'Deadline',
              backgroundColor: Colors.red,
              // onTap:() {
              //   Navigator.push(context,MaterialPageRoute(builder: (_) => DeadlineTable(title: 'DeadlineTable',)));
              //   },
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.note),
              label: 'Catatan',
              backgroundColor: Colors.red,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.people),
              label: 'Contact Person',
              backgroundColor: Colors.green,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.note),
              label: 'Review',
              backgroundColor: Colors.yellow,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.login),
              label: 'Login',
              backgroundColor: Colors.black,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              label: 'Register',
              backgroundColor: Colors.black,
            ),
          ],
        )
        // BottomAppBar(
        //   color: Colors.blue,
        //   child: IconButton(
        //     icon: Icon(Icons.note),
        //     onPressed: () {},
        //   ),
        // ),
        );
  }
}
