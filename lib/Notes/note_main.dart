import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:reminder/Notes/card_notes.dart';
import 'package:reminder/Notes/form_notes.dart';
import 'package:reminder/Notes/note.dart';
// import 'package:uas_project/screens/lokasi_vaksin_form.dart';

void main() {
  runApp(const DaftarCatatan());
}

bool pertama = true;

class DaftarCatatan extends StatelessWidget {
  const DaftarCatatan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return MaterialApp(
        home: const Scaffold(
      body: CardCatatanx(),
    ));
  }
}

class CardCatatanx extends StatefulWidget {
  const CardCatatanx({Key? key}) : super(key: key);

  @override
  CardCatatanState createState() {
    return CardCatatanState();
  }
}

class CardCatatanState extends State<CardCatatanx> {
  Future<List<Catetans>> _fetchData(String query) async {
    var response = await http
        .get(Uri.parse('https://reminder-tk-pbp.herokuapp.com/notes/json'));

    List<Catetans> notes = CatetansFromJson(response.body).Catatan;
    notes = notes.where((cek) {
      final matkulLower = cek.MataKuliah.toLowerCase();
      final mingguLower = cek.MingguKe.toLowerCase();
      final queryLower = query.toLowerCase();

      return matkulLower.contains(queryLower) ||
          mingguLower.contains(queryLower);
    }).toList();

    return notes;
  }

  final _controller = TextEditingController();
  String query = "";
  List<Catetans> notesList = [];
  Timer? debouncer;

  // Source: https://github.com/JohannesMilke/filter_listview_example
  @override
  void initState() {
    if (pertama) {
      stateAwal();
    }
    super.initState();
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 1000),
  }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }
    debouncer = Timer(duration, callback);
  }

  Future searchCatatan(String search) async => debounce(() async {
        final notess = await _fetchData(search);

        setState(() {
          query = query;
          notesList = notess;
        });
      });

  FutureOr onGoBack(dynamic value) {
    stateAwal();
  }

  Future stateAwal() async => debounce(() async {
        if (!mounted) return;
        final notesx = await _fetchData(query);

        setState(() {
          notesList = notesx;
        });
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("CATATAN"),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 65,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const <Widget>[
                Text(
                  "Catatan",
                  style: TextStyle(
                      fontSize: 30,
                      color: Colors.red,
                      fontWeight: FontWeight.w700),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(5),
            child: TextField(
                controller: _controller,
                onChanged: (text) {
                  searchCatatan(text);
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5)),
                  prefixIcon: const Icon(Icons.search, color: Colors.grey),
                  suffixIcon: IconButton(
                      icon: const Icon(Icons.clear, color: Colors.grey),
                      onPressed: () {
                        _controller.clear();
                        searchCatatan("");
                      }),
                  hintText: 'Cari catatan',
                )),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: FutureBuilder<List<Catetans>>(
                future: _fetchData(query),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Scrollbar(
                        isAlwaysShown: true,
                        child: ListView.builder(
                            itemCount: notesList.length,
                            itemBuilder: (context, index) {
                              return CardCatatan(
                                  notesList[index], context, onGoBack);
                            }));
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }

                  return Center(
                    child: Column(
                      children: const [
                        SizedBox(height: 100),
                        Text("Loading Catatan"),
                        SizedBox(height: 25),
                        SizedBox(
                            width: 30,
                            height: 30,
                            child: CircularProgressIndicator()),
                      ],
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const FormCatatan()),
        ).then(onGoBack),
        child: const Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
