import 'dart:convert';

Notes CatetansFromJson(String str) => Notes.fromJson(json.decode(str));
String CatetansToJson(Notes data) => json.encode(data.toJson());

class Notes {
  Notes({
    required this.Catatan,
  });

  List<Catetans> Catatan;

  factory Notes.fromJson(Map<String, dynamic> json) => Notes(
        Catatan: List<Catetans>.from(
            json["Catatan"].map((x) => Catetans.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Catatan": Catatan[0].toJson(),
      };
}

class Catetans {
  final String MataKuliah;
  final String MingguKe;
  final String Materi;
  final String Catetan;

  Catetans({
    required this.MataKuliah,
    required this.MingguKe,
    required this.Materi,
    required this.Catetan,
  });

  factory Catetans.fromJson(Map<String, dynamic> json) => Catetans(
        MataKuliah: json['MataKuliah'],
        MingguKe: json['MingguKe'],
        Materi: json['Materi'],
        Catetan: json['Catetan'],
      );

  Map<String, dynamic> toJson() => {
        "MataKuliah": MataKuliah,
        "MingguKe": MingguKe,
        "Materi": Materi,
        "Catetan": Catetan,
      };
}
