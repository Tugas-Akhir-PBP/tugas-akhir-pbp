import 'package:flutter/material.dart';
import 'package:reminder/Notes/note.dart';

//buat card
Widget CardCatatan(catetan, context, onGoBack) {
  return Card(
    child: Column(children: <Widget>[
      Text(
        catetan.MataKuliah,
        style: TextStyle(
            fontSize: 30, color: Colors.blue, fontWeight: FontWeight.bold),
      ),
      const SizedBox(height: 15),
      Text(
        'minggu-Ke: ' + catetan.MingguKe,
        style: TextStyle(fontSize: 25, color: Colors.blue),
      ),
      const SizedBox(height: 15),
      Text(
        catetan.Materi,
        style: TextStyle(fontSize: 20, color: Colors.blue),
      ),
      const SizedBox(height: 15),
      Text(
        catetan.Catetan,
        style: TextStyle(fontSize: 15, color: Colors.blue),
      ),
    ]),
  );
}
