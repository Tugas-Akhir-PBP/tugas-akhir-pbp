import 'dart:convert';
import 'package:select_form_field/select_form_field.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:reminder/Notes/note.dart';
import 'dart:ui';

class FormCatatan extends StatefulWidget {
  const FormCatatan({Key? key}) : super(key: key);

  @override
  FormCatatanState createState() {
    return FormCatatanState();
  }
}

class FormCatatanState extends State<FormCatatan> {
  String finalResponse = "";
  final _formKey = GlobalKey<FormState>();

  final List<Map<String, dynamic>> pilihanMatkul = [
    {'label': 'SDA', 'value': 'SDA'},
    {'label': 'PBP', 'value': 'PBP'},
    {'label': 'OS', 'value': 'OS'},
  ];

  final List<Map<String, dynamic>> pilihanMinggu = [
    {'label': "1", 'value': "1"},
    {'label': "2", 'value': "2"},
    {'label': "3", 'value': "3"},
    {'label': "4", 'value': "4"},
    {'label': "5", 'value': "5"},
    {'label': "6", 'value': "6"},
    {'label': "7", 'value': "7"},
    {'label': "8", 'value': "8"},
    {'label': "9", 'value': "9"},
    {'label': "10", 'value': "10"},
    {'label': "11", 'value': "11"},
  ];

  Future<Notes> createCatetan(
      String MataKuliah, String MingguKe, String Materi, String Catetan) async {
    List<Catetans> catatanz = [];
    Catetans catetanBaru = Catetans(
        MataKuliah: MataKuliah,
        MingguKe: MingguKe,
        Materi: Materi,
        Catetan: Catetan);
    catatanz.add(catetanBaru);
    Notes data = Notes(Catatan: catatanz);

    final response = await http.post(
      Uri.parse('https://reminder-tk-pbp.herokuapp.com/notes/json'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: CatetansToJson(data),
    );

    if (response.statusCode == 200) {
      Navigator.of(context).pop();
      return Notes.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed');
    }
  }

  Future<void> saveCatetan() async {
    final validation = _formKey.currentState!.validate();
    if (!validation) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Penambahan Catatan Gagal')),
      );
    }
    _formKey.currentState!.save();
    createCatetan(mataKuliah, mingguKe, materi, catetan);
  }

  String mataKuliah = "";
  String mingguKe = "";
  String materi = "";
  String catetan = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tambah Catatan'),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Scrollbar(
            isAlwaysShown: true,
            child: ListView(
              children: [
                SizedBox(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const <Widget>[
                      Text(
                        "Tambah",
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.blue,
                            fontWeight: FontWeight.w700),
                      ),
                      Text(
                        "Catatan",
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.red,
                            fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 15),
                SelectFormField(
                    decoration: const InputDecoration(
                      labelText: "Pilih Mata Kuliah",
                      border: OutlineInputBorder(),
                    ),
                    items: pilihanMatkul,
                    onChanged: (val) => setState(() {
                          mataKuliah = val;
                        })),
                const SizedBox(height: 15),
                SelectFormField(
                    decoration: const InputDecoration(
                      labelText: "Pilih Minggu",
                      border: OutlineInputBorder(),
                    ),
                    items: pilihanMinggu,
                    onChanged: (val) => setState(() {
                          mingguKe = val;
                        })),
                const SizedBox(height: 15),
                TextFormField(
                  decoration: const InputDecoration(
                      labelText: "Materi",
                      border: OutlineInputBorder(),
                      errorBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.red, width: 1.5))),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Masukkan Materi';
                    }
                    materi = value;
                  },
                  onSaved: (value) {},
                ),
                const SizedBox(height: 15),
                TextFormField(
                  decoration: const InputDecoration(
                      labelText: "Catatan",
                      border: OutlineInputBorder(),
                      errorBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.red, width: 1.5))),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Masukkan Catatan';
                    }
                    catetan = value;
                  },
                  onSaved: (value) {},
                ),
                const SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () async {
                    saveCatetan();
                  },
                  child: const Text('Submit'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
