import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:reminder/deadline_table/models/model.dart';
import 'package:reminder/deadline_table/deadline_table.dart';



class AssignmentCard extends StatelessWidget {
  final Model model;
  final Function(int id) deleteTable;
  const AssignmentCard(this.model, this.deleteTable, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Container(
            color: Color(0xFF313E58),
            padding: EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                  title: Text(model.namaMatkul,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          color: Colors.white)),
                  subtitle: Text("Assignment: " + model.namaTugas + "\nDeadline: "+ model.tglKumpul + "\nPriority: "+ model.skalaPrioritas,
                      style: TextStyle(
                          fontWeight: FontWeight.w200,
                          fontSize: 18.0,
                          color: Colors.grey[200])),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    TextButton(
                      child: const Text('REMOVE'),
                      onPressed: () async {
                        var id = model.pk;
                        deleteTable(id);
                        },
                    ),
                    const SizedBox(width: 8),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
