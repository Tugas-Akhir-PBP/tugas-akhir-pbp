import 'dart:convert';

class Model {
  final int pk;
  final String namaMatkul;
  final String namaTugas;
  final String tglKumpul;
  final String skalaPrioritas;

  Model({required this.pk, required this.namaMatkul,required this.namaTugas,required this.tglKumpul,required this.skalaPrioritas});

  factory Model.fromRawJson(String str) => Model.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Model.fromJson(Map<String, dynamic> json) => Model(
        pk: json["pk"],        
        namaMatkul: json["fields"]["namaMatkul"],
        namaTugas: json["fields"]["namaTugas"],
        tglKumpul: json["fields"]["tglKumpul"],
        skalaPrioritas: json["fields"]["skalaPrioritas"]
      );

  Map<String, dynamic> toJson() => {
        "pk": pk,
        "namaMatkul": namaMatkul,
        "namaTugas": namaTugas,
        "tglKumpul": tglKumpul,
        "skalaPrioritas": skalaPrioritas,
      };
}