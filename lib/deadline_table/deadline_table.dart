import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'components/assignment_card.dart';
import 'package:reminder/CookieRequest.dart';
import 'dart:convert' as convert;

import 'models/model.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          color: Color(0xFF313E58),
        ),
        scaffoldBackgroundColor: const Color(0xFF5965A4),
        fontFamily: 'Georgia',
        textTheme: const TextTheme(
          headline6: TextStyle(
              fontSize: 30.0, fontFamily: 'Arial', fontWeight: FontWeight.bold),
          bodyText2: TextStyle(
              fontSize: 20.0, fontFamily: 'Arial', color: Colors.white),
          bodyText1: TextStyle(
              fontSize: 18.0,
              fontFamily: 'Arial',
              color: Colors.black,
              fontStyle: FontStyle.italic),
        ),
      ),
      home: const DeadlineTable(title: 'Deadline Table'),
    );
  }
}

class DeadlineTable extends StatefulWidget {
  const DeadlineTable({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<DeadlineTable> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<DeadlineTable> {
  static List<Model> tabel = [];
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: [
                const SizedBox(
                  height: 80.0,
                  child: DrawerHeader(
                      child: Center(
                          child: Text('Reminder Bar',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25.0,
                                  fontFamily: 'Arial',
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.normal))),
                      decoration: BoxDecoration(color: Color(0xFF313E58)),
                      margin: EdgeInsets.all(0.0),
                      padding: EdgeInsets.all(0.0)),
                ),
                ListTile(
                  title: const Text('Add Table'),
                  onTap: () {
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.push(
                      context,
                      MaterialPageRoute<void>(
                        builder: (BuildContext context) {
                          return Scaffold(
                            appBar: AppBar(
                              title: const Text('Add Table',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25.0,
                                      fontFamily: 'Arial',
                                      fontWeight: FontWeight.bold,
                                      fontStyle: FontStyle.normal)),
                            ),
                            body: const AddTable(),
                          );
                        },
                      ),
                    );
                  },
                ),
              ])),
      body:  Container(
              child: FutureBuilder(
                future: fetching(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<Model>? ambilTabel = snapshot.data as List<Model>;
                    tabel = ambilTabel;
                    return ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: ambilTabel.length, 
                      itemBuilder: (context, index) {  
                        return AssignmentCard(ambilTabel[index], deleteTable);
                      },                                      
                    );
                  } else if (snapshot.hasError) {
                    return Text("-->>${snapshot.error}<<--");
                  }
                  return CircularProgressIndicator();
                },
              ),
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute<void>(
              builder: (BuildContext context) {
                return Scaffold(
                  appBar: AppBar(
                    title: const Text('Add Table',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontFamily: 'Arial',
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.normal)),
                  ),
                  body: const AddTable(),
                );
              },
            ),
          );
        },
        child: const Icon(Icons.add),
        backgroundColor: Colors.blue[800],
      ),
    );
  }
    Future<List<Model>> fetching() async {
    final request = context.watch<CookieRequest>();
    String url = 'https://reminder-tk-pbp.herokuapp.com/deadline-table/getTabelFlutter';

    final response = await request.get(url);
    List<Model> result = [];
    for (var d in response) {
      if (d != null) {
        result.add(Model.fromJson(d));
      }
    }
    return result;
  }
    Future<List<Model>> deleteTable(id) async {
    final request = context.watch<CookieRequest>();
    String url = 'https://reminder-tk-pbp.herokuapp.com/deadline-table/getTabelFlutter/';

    final response = await request.get(url);
    List<Model> result = [];
    for (var d in response) {
      if (d != null && d == id) {
        result.remove(Model.fromJson(d));
      }
    }
    return result;
    }
}

class ScrollableWidget extends StatelessWidget {
  //SCROLLABLE
  final Widget child;

  const ScrollableWidget(
      Container container,
      SizedBox sizedBox, {
        Key? key,
        required this.child,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        child: child,
      ),
    );
  }
}

class AddTable extends StatefulWidget {
  const AddTable({Key? key}) : super(key: key);

  @override
  AddTableSt createState() {
    return AddTableSt();
  }
}

class AddTableSt extends State<AddTable> {
  final _formKey = GlobalKey<FormState>();
  String namaMatkul = "";
  String namaTugas = "";
  String tglKumpul = "";
  String skalaPrioritas = "";

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Color(0x04AA6DFF);
    }
    return Color(0x04AA6DFF);
  }

  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();
    return Container(
        padding: EdgeInsets.all(24),
        child: Card(
            child: Container(
                padding: EdgeInsets.all(24),
                color: Color(0xFF313E58),
                height: 300.0,
                child: Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      TextFormField(
                        decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Arial',
                                color: Colors.white),
                            hintText: 'Subject\'s name'),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: 'Arial',
                            color: Colors.white),
                        onSaved: (String? value) {
                          // This optional block of code can be used to run
                          // code when the user saves the form.
                          setState(() {
                            namaMatkul = value!;
                          });
                        },
                        onChanged: (String? value) {
                          setState(() {
                            namaMatkul = value!;
                          });
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Mohon diisi';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Arial',
                                color: Colors.white),
                            hintText: 'Assignment\'s name'),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: 'Arial',
                            color: Colors.white),
                        onSaved: (String? value) {
                          // This optional block of code can be used to run
                          // code when the user saves the form.
                          setState(() {
                            namaTugas = value!;
                          });
                        },
                        onChanged: (String? value) {
                          setState(() {
                            namaTugas = value!;
                          });
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Mohon diisi';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Arial',
                                color: Colors.white),
                            hintText: 'YYYY-MM-DD'),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: 'Arial',
                            color: Colors.white),
                        onSaved: (String? value) {
                          // This optional block of code can be used to run
                          // code when the user saves the form.
                          setState(() {
                            tglKumpul = value!;
                          });
                        },
                        onChanged: (String? value) {
                          setState(() {
                            tglKumpul = value!;
                          });
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                            hintStyle: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Arial',
                                color: Colors.white),
                            hintText: 'Priority list'),
                        style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: 'Arial',
                            color: Colors.white),
                        onSaved: (String? value) {
                          // This optional block of code can be used to run
                          // code when the user saves the form.
                          setState(() {
                            skalaPrioritas = value!;
                          });
                        },
                        onChanged: (String? value) {
                          setState(() {
                            skalaPrioritas = value!;
                          });
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Column(
                            children: [
                              ElevatedButton(
                                onPressed: () async {
                                  // Validate returns true if the form is valid, or false otherwise.
                                  if (_formKey.currentState!.validate()) {
                                    // If the form is valid, display a snackbar. In the real world,
                                    // you'd often call a server or save the information in a database.
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(content: Text('Processing Data')),
                                    );
                                    final response = await request.post(
                                    "https://reminder-tk-pbp.herokuapp.com/deadline-table/getTabelFlutter",
                                    convert.jsonEncode(<String, String>{
                                      'namaMatkul': namaMatkul,
                                      'namaTugas': namaTugas,
                                      'tglKumpul' : tglKumpul,
                                      'skalaPrioritas' : skalaPrioritas,
                                    }));
                                  if (response['status'] == 'success') {
                                    Navigator.push(
                                        context, MaterialPageRoute(builder: (_) =>MyApp()));
                                  }
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                  MaterialStateProperty.resolveWith(
                                      getColor),

                                ),
                                child: const Text('Submit',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontFamily: 'Arial',
                                        fontStyle: FontStyle.normal)),
                              ),
                            ],
                          )),
                    ],
                  ),
                ))));
  
  }

}
