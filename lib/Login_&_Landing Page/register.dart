import 'dart:convert';
// import 'dart:html';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:reminder/CookieRequest.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:email_validator/email_validator.dart';

void main() => runApp(MaterialApp(
      title: "Register",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: RegisterPages(),
    ));

void showError(context, Map errors) {
  showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Center(child: Text(errors['message'].toString())),
            ),
          ],
        );
      });
}

class RegisterPages extends StatefulWidget {
  static const routeName = '/register';

  const RegisterPages({Key? key}) : super(key: key);

  @override
  RegisterPagesState createState() => RegisterPagesState();
}

class RegisterPagesState extends State<RegisterPages> {
  final formKey = GlobalKey<FormState>();

  bool _obscureText = true;

  void toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  String _username = '';
  String _email = '';
  String _password = '';

  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();

    return Scaffold(
        appBar: AppBar(
          title: Text('Register'),
        ),
        resizeToAvoidBottomInset: false,
        //backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
              padding: const EdgeInsets.all(20),
              child: Column(children: <Widget>[
                const SizedBox(
                  height: 80,
                ),
                Text(
                  '\n Register Here',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Color.fromRGBO(27, 142, 255, 1)),
                ),
                const SizedBox(
                  height: 60,
                ),
                Form(
                    key: formKey,
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: TextFormField(
                            onChanged: (String value) {
                              _username = value;
                            },
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Username',
                            ),
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Username field cannot be empty!";
                              }
                              return null;
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: TextFormField(
                            onChanged: (String value) {
                              _email = value;
                            },
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Email',
                            ),
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Email field cannot be empty!";
                              } else if (!EmailValidator.validate(value)) {
                                return "Email is not valid!";
                              }
                              return null;
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: TextFormField(
                              onChanged: (String value) {
                                _password = value;
                              },
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Password',
                              ),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Password field cannot be empty!";
                                } else if (value.length < 8) {
                                  return "Password must contain at least 8 characters";
                                }
                                return null;
                              },
                              obscureText: _obscureText,
                            )),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                          child: ElevatedButton(
                            onPressed: toggle,
                            child: new Text(
                                _obscureText
                                    ? "Show Password"
                                    : "Hide Password",
                                style: TextStyle(
                                  fontSize: 14,
                                )),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.blueAccent),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                            ),
                          ),
                          alignment: Alignment.centerRight,
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                          // width: double.infinity,
                          child: ElevatedButton(
                            style:
                                ElevatedButton.styleFrom(primary: Colors.white),
                            onPressed: () async {
                              // final response = await request.login('http://localhost:8000/login', {
                              //   'username': _username,
                              //   'password': _password,
                              // });

                              // if (request.loggedIn) {
                              //   Navigator.of(context).pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
                              // } else {
                              //   showError(context, response);
                              // }
                              if (formKey.currentState!.validate()) {
                                final response = await request.post(
                                    'https://reminder-tk-pbp.herokuapp.com/login/register/',
                                    jsonEncode(<String, String>{
                                      'username': _username,
                                      'email': _email,
                                      'password': _password,
                                    }));

                                if (response['status'] == true) {
                                  await request.login(
                                      'https://reminder-tk-pbp.herokuapp.com/login',
                                      {
                                        'username': _username,
                                        'password': _password,
                                      });
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/', (Route<dynamic> route) => false);
                                }
                              }
                            },
                            child: const Text('Register',
                                style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 18,
                                )),
                          ),
                        ),
                      ],
                    )),
              ])),
        ));
  }
}
