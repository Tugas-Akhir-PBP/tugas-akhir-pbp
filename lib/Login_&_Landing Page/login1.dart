import 'dart:convert';
// import 'dart:html';
import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:http/http.dart' as http;
import 'package:reminder/CookieRequest.dart';
//import 'package:reminder/Login%20&%20Landing%20Page/login.dart';
import 'package:reminder/main.dart';
import 'package:reminder/Login_&_Landing Page/register.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// void main() {
//   runApp(const MyReminder());
// }

// class MyReminder extends StatelessWidget {
//   const MyReminder({Key? key}) : super(key: key);

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     const appTitle = 'Welcome to Reminder!';
//     const title1 = 'Login';

//     // return MaterialApp(
//     //   title: appTitle,
//     //   theme: ThemeData(
//     //     primarySwatch: Colors.lightBlue,
//     //   ),
//     //   home: Scaffold(
//     //     appBar: AppBar(
//     //       title: Text(title1, style: TextStyle(color: Colors.white)),
//     //     ),
//     //     body: const LoginPages(),
//     //   ),
//     // );
//     return Provider(
//         create: (_) {
//           CookieRequest request = CookieRequest();

//           return request;
//         },
//         child: MaterialApp(
//           title: appTitle,
//           debugShowCheckedModeBanner: false,
//           theme: ThemeData(
//             primarySwatch: Colors.blue,
//           ),

//           routes: {
//             '/': (ctx) => const MyHomePage(title: appTitle),
//             LoginPages.routeName: (ctx) => const LoginPages(),
//             '/login': (BuildContext context) => const LoginPages()},
//         ));
//   }
// }

void main() => runApp(MaterialApp(
      title: "Login",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: LoginPages(),
    ));

void showError(context, Map errors) {
  showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Center(child: Text(errors['message'].toString())),
            ),
          ],
        );
      });
}

class LoginPages extends StatefulWidget {
  static const routeName = '/login';

  const LoginPages({Key? key}) : super(key: key);

  @override
  LoginPagesState createState() => LoginPagesState();
}

class LoginPagesState extends State<LoginPages> {
  final formKey = GlobalKey<FormState>();

  bool _obscureText = true;

  void toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  String _username = '';
  String _password = '';

  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();

    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        resizeToAvoidBottomInset: false,
        //backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
              padding: const EdgeInsets.all(20),
              child: Column(children: <Widget>[
                const SizedBox(
                  height: 80,
                ),
                Text(
                  '\n Please Login to Continued',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Color.fromRGBO(27, 142, 255, 0.5)),
                ),
                const SizedBox(
                  height: 60,
                ),
                Form(
                    key: formKey,
                    child: Column(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: TextFormField(
                            onChanged: (String value) {
                              _username = value;
                            },
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Username',
                            ),
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "Username field cannot be empty!";
                              }
                              return null;
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: TextFormField(
                              onChanged: (String value) {
                                _password = value;
                              },
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Password',
                              ),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Password field cannot be empty!";
                                } else if (value.length < 8) {
                                  return "Password must contain at least 8 characters";
                                }
                                return null;
                              },
                              obscureText: _obscureText,
                            )),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                          child: ElevatedButton(
                            onPressed: toggle,
                            child: new Text(
                                _obscureText
                                    ? "Show Password"
                                    : "Hide Password",
                                style: TextStyle(
                                  fontSize: 14,
                                )),
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.blueAccent),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                            ),
                          ),
                          alignment: Alignment.centerRight,
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                          // width: double.infinity,
                          child: ElevatedButton(
                            style:
                                ElevatedButton.styleFrom(primary: Colors.white),
                            onPressed: () async {
                              // final response = await request.login('http://localhost:8000/login', {
                              //   'username': _username,
                              //   'password': _password,
                              // });

                              // if (request.loggedIn) {
                              //   Navigator.of(context).pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
                              // } else {
                              //   showError(context, response);
                              // }
                              if (formKey.currentState!.validate()) {
                                final response = await request.login(
                                    'https://reminder-tk-pbp.herokuapp.com/loginauth',
                                    {
                                      'username': _username,
                                      'password': _password,
                                    });

                                if (request.loggedIn) {
                                  // Navigator.of(context).pushNamedAndRemoveUntil(
                                  //     '/', (Route<dynamic> route) => false);

                                  // Move to AuthHomePage
                                  Navigator.push(
                                      context,
                                      new MaterialPageRoute(
                                        builder: (context) => new MainPage(),
                                      ));
                                } else {
                                  showError(context, response);
                                }
                              }
                            },
                            child: const Text('Login',
                                style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 18,
                                )),
                          ),
                        ),
                        const SizedBox(
                          height: 60,
                        ),
                        RichText(
                          text: TextSpan(
                              text: "Don't have account yet? Register here",
                              style: const TextStyle(
                                color: Colors.lightBlueAccent,
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.restorablePopAndPushNamed(
                                      context, RegisterPages.routeName);
                                }),
                        )
                      ],
                    )),
              ])),
        ));
  }
}
