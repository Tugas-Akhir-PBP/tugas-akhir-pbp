import 'dart:convert';
//import 'dart:html';
import 'package:http/http.dart' as http;
import 'package:reminder/CookieRequest.dart';
import 'package:reminder/main.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// List<dynamic> extractedData = [];
// Future<void> fetchData() async {
//   const url = 'http://https://reminder-tk-pbp.herokuapp.com/';
//   try {
//     final response = await http.get(Uri.parse(url));
//     extractedData = jsonDecode(response.body);
//     print(extractedData);
//   } catch (error) {
//     print(error);
//   }
// }

class MyHomePageForm extends StatefulWidget {
  static const routeName = '/login';
  const MyHomePageForm({Key? key}) : super(key: key);

  @override
  MyHomePage createState() => MyHomePage();
}

class MyHomePage extends State<MyHomePageForm> {
  final _formkey = GlobalKey<FormState>();

  bool _obscureText = true;
  String _username = "";
  String _password = "";

  void toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  // Masih harus atur designnya biar responsive ke semua devices !!!
  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();
    return Form(
      key: _formkey,
      child: Column(children: <Widget>[
        // ignore: prefer_const_constructors
        Text(
          '\n Please Login to Continued',
          textAlign: TextAlign.center,
          style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
              color: Color.fromRGBO(27, 142, 255, 0.5)),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 25),
          child: TextFormField(
            onChanged: (String value) {
              _username = value;
            },
            autofocus: true,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Username',
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Username field cannot be empty!";
              }
              return null;
            },
            //onSaved: (value) => _username = value!,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 500, vertical: 25),
          child: TextFormField(
            onChanged: (String value) {
              _password = value;
            },
            autofocus: true,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Password',
            ),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Password field cannot be empty!";
              } else if (value.length < 8) {
                return 'Password too short.';
              }
              return null;
            },
            // onSaved: (value) => _password = value!,
            obscureText: _obscureText,
          ),
        ),
        Padding(
            padding: EdgeInsets.fromLTRB(1300, 20, 20, 10),
            child: ElevatedButton(
              onPressed: toggle,
              child: new Text(_obscureText ? "Show Password" : "Hide Password"),
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.blueAccent),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
            )),
        Padding(
          // padding: EdgeInsets.symmetric(horizontal: 700, vertical: 50),
          // child: SizedBox(
          //   height: 40,
          //   width: 100,
          //   child: TextButton(
          //     style: ButtonStyle(
          //       backgroundColor:
          //           MaterialStateProperty.all<Color>(Colors.blueAccent),
          //       foregroundColor:
          //           MaterialStateProperty.all<Color>(Colors.white),
          //     ),
          //     onPressed: () {},
          //     child: Text('Login'),
          //   ),
          // )
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.blueAccent),
              onPressed: () async {
                if (_formkey.currentState!.validate()) {
                  // ScaffoldMessenger.of(context).showSnackBar(
                  //     const SnackBar(content: Text('Logging in ...')));
                  // final response = await http.post(
                  //     Uri.parse(
                  //         "https://reminder-tk-pbp.herokuapp.com/login/"),
                  //     headers: <String, String>{
                  //       'Content-Type': 'application/json;charset=UTF-8',
                  //     },
                  //     body: jsonEncode(<String, String>{
                  //       'username': _username,
                  //       'password': _password,
                  //     }));
                  // print(response);

                  final response = await request
                      .login("https://reminder-tk-pbp.herokuapp.com/login", {
                    'username': _username,
                    'password': _password,
                  });

                  // handling response selanjutnya mau ngapain
                  if (request.loggedIn) {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        '/ ', (Route<dynamic> route) => false);
                  } else {
                    //popup message: failed

                  }
                }
              },
              child: const Text(
                'Login',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
            ),
          ),
        )
      ]),
    );
  }
}
